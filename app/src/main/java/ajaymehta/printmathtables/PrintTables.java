package ajaymehta.printmathtables;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class PrintTables {

    public static void printNumbers(){

        for(int i=2; i<=5; i++){  // i is here for tables ... 2 ,3 4 ..so on

            for(int j=1; j<=10; j++) {

                System.out.println(i+" X "+j+" = "+i*j);  // j is for mulitplication  1 to 10...to tables 2,3,4..
            } // end of j looop

            System.out.println("====================");

        }  // end of i loop


    }

    public static void main(String args[]) {

        printNumbers();


    }
}
